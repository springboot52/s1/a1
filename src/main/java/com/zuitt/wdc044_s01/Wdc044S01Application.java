package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.print.DocFlavor;

@SpringBootApplication
@RestController
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name",defaultValue = "World")String name){
		return "Hello "+name+"!";
	}

//	ACTIVITY
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name",defaultValue = "user")String name){
		String result=String.format("Hi %s!",name);
		return result;
	}


}
